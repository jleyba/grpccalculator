package main

import (
	pb "bitbucket.com/go/calculatorservicemodel"
	"context"
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"math/big"
	"net"
	"strconv"
	"strings"
	"time"
	// _ "net/http/pprof"
)

// Convert String to int
func StrToInt(str string) (int, error) {
	nonFractionalPart := strings.Split(str, ".")
	return strconv.Atoi(nonFractionalPart[0])
}

// Calculate factorial with iterative method
func factorialIter(x int32) *big.Int {
	result := big.NewInt(1)
	var i int32
	for i = 2; i <= x; i++ {
		result.Mul(result, big.NewInt(int64(i)))
	}

	return result
}

// Calculates factorial recursive
func factorialRecursive(x *big.Int) *big.Int {
	n := big.NewInt(1)
	if x.Cmp(big.NewInt(0)) == 0 {
		return n
	}
	return n.Mul(x, factorialRecursive(n.Sub(x, n)))
}

// Implements interface
type CalculatorServiceServer struct {
}

// Echo service impl.
func (*CalculatorServiceServer) Echo(ctx context.Context, in *pb.EchoRequest) (*pb.EchoResponse, error) {
	return &pb.EchoResponse{Message: in.Message}, nil
}

// Factorial Iteravive service impl.
func (*CalculatorServiceServer) FactorialIterative(ctx context.Context,
	in *pb.FactorialRequest) (*pb.FactorialResponse, error) {

	return &pb.FactorialResponse{Factorial: []byte(factorialIter(in.Number).String())}, nil
}

// Display start messages
func start() {

	fmt.Println("Starting server")

	// Start to read conf file
	fmt.Print("\n\n")
	fmt.Println("=============================================")
	fmt.Println(" Configuration checking - gprCalculator v0.1")
	fmt.Println("=============================================")

	// loading configuration
	viper.SetConfigName("conf")          // name of config file (without ext)
	viper.AddConfigPath(".")             // default path for conf file
	viper.SetDefault("port", ":9596")    // default port value
	viper.SetDefault("loglevel", "info") // default port value

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		fmt.Printf("Fatal error config file: %v \n", err)
		panic(err)
	}

	fmt.Println("-- Using port:              ", viper.GetString("port"))

	zerolog.TimeFieldFormat = time.RFC3339
	zerolog.TimestampFieldName = "@timestamp"
	switch viper.GetString("loglevel") {
	case "info":
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	case "error":
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	case "debug":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	case "disabled":
		zerolog.SetGlobalLevel(zerolog.Disabled)
	default:
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	fmt.Println("-- Log lever set to:        ", viper.GetString("loglevel"))

	fmt.Println("=============================================")

}

func main() {

	start()

	// Used for profiling
	/*go func() {
		fmt.Println(http.ListenAndServe("localhost:6060", nil))
	}()*/

	fmt.Println("Started server...")
	lis, err := net.Listen("tcp", viper.GetString("port"))
	if err != nil {
		log.Fatal().Msgf("failed to listen: %v", err)
	}
	// Creates a new gRPC server
	s := grpc.NewServer()
	pb.RegisterCalculatorServiceServer(s, &CalculatorServiceServer{})
	s.Serve(lis)

}
