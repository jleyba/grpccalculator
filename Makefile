normal: clean
	go build

# Generate code for linux
linux: clean
	env GOOS=linux GOARCH=amd64 go build
	zip -9 grpccalculator_linux_v1.0.zip grpccalculator conf.yaml

# Generate code for windows
windows:
	env GOOS=windows GOARCH=amd64 go build
	mv grpccalculator.exe grpccalculator_amd64.exe
	zip -9 grpccalculator_win_v1.0.zip grpccalculator_amd64.exe conf.yaml

# Clean all old files
clean:
	rm -f grpccalculator
	rm -f *.exe

